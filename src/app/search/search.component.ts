import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  public searchField = '';

  @Output() public searchQuery = new EventEmitter<string>();

  constructor() {}

  public search() {
    this.searchQuery.emit(this.searchField);
  }

}

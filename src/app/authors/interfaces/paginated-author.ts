import { IMeta } from 'src/app/pagination';

import { IAuthor } from './author';

export interface IPaginatedAuthor {
    authors: IAuthor[],
    meta: IMeta,
}

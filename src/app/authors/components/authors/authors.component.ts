import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject, takeUntil } from 'rxjs';

import { IMeta } from 'src/app/pagination';

import { IAuthor } from '../../interfaces/author';
import { IPaginatedAuthor } from '../../interfaces/paginated-author';
import { AuthorsService } from '../../services/authors.service';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css'],
})
export class AuthorsComponent implements OnInit, OnDestroy {
  
  public authors: IAuthor[] = [];
  public meta = <IMeta>{};
  public displayedColumns: string[] = ['id', 'first_name', 'last_name', 'actions'];
  
  private readonly _destroy$ = new Subject<void>();

  constructor(public authorsService: AuthorsService) {}

  public ngOnInit(): void {
    this._getAuthors();
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public changePage(page: number): void {
    this._getAuthors(page);
  }

  private _getAuthors(page: number = 1): void {
    this.authorsService.getAuthors(page)
      .pipe(
        takeUntil(this._destroy$),
      )
      .subscribe(({ authors, meta }: IPaginatedAuthor) => { 
        this.authors = authors;
        this.meta = meta;
      });
  }

}

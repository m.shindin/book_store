import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IAuthor } from '../interfaces/author';
import { IPaginatedAuthor } from '../interfaces/paginated-author';

@Injectable({
  providedIn: 'root',
})
export class AuthorsService {

  private _api = '/api/authors';
  private _httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private _http: HttpClient) {}

  public getAuthors(page: number): Observable<IPaginatedAuthor> {
    return this._http.get<IPaginatedAuthor>(`${this._api}?page=${page}`);
  }

  public getAuthor(id: number): Observable<IAuthor> {
    return this._http.get<IAuthor>(`${this._api}/${id}`);
  }

  public createAuthor(author: IAuthor): Observable<IAuthor> {
    return this._http.post<IAuthor>(`${this._api}`, author, this._httpOptions);
  }

  public updateAuthor(id: number): Observable<IAuthor> {
    return this._http.put<IAuthor>(`${this._api}/${id}`, this._httpOptions);
  }

  public deleteAuthor(id: number): Observable<IAuthor> {
    return this._http.delete<IAuthor>(`${this._api}/${id}`, this._httpOptions);
  }
  
}

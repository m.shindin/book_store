import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';

import { PaginationModule } from '../pagination';

import { AuthorsComponent } from './components/authors/authors.component';
import { AuthorsRoutingModule } from './authors-routing.module';


@NgModule({
  declarations: [AuthorsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    PaginationModule,
    AuthorsRoutingModule,
  ],
})
export class AuthorsModule {}

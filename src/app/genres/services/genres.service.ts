import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IGenre } from '../interfaces/genre';
import { IPaginatedGenre } from '../interfaces/paginated-genre';

@Injectable({
  providedIn: 'root',
})
export class GenresService {

  private _api = '/api/genres';
  private _httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private _http: HttpClient) {}

  public getGenres(page: number): Observable<IPaginatedGenre> {
    return this._http.get<IPaginatedGenre>(`${this._api}?page=${page}`);
  }

  public getGenre(id: number): Observable<IGenre> {
    return this._http.get<IGenre>(`${this._api}/${id}`);
  }

  public createGenre(genre: IGenre): Observable<IGenre> {
    return this._http.post<IGenre>(this._api, genre, this._httpOptions);
  }
  
}

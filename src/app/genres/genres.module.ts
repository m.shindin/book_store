import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

import { PaginationModule } from '../pagination';

import { GenresComponent } from './components/genres/genres.component';
import { GenresRoutingModule } from './genres-routing.module';


@NgModule({
  declarations: [GenresComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatIconModule,
    PaginationModule,
    GenresRoutingModule,
  ],
})
export class GenresModule {
  constructor() {}
}

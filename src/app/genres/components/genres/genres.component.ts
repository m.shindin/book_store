import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject, takeUntil } from 'rxjs';

import { IMeta } from 'src/app/pagination';

import { IGenre } from '../../interfaces/genre';
import { IPaginatedGenre } from '../../interfaces/paginated-genre';
import { GenresService } from '../../services/genres.service';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css'],
})
export class GenresComponent implements OnInit, OnDestroy {
  
  public genres: IGenre[] = [];
  public meta = <IMeta>{};

  private readonly _destroy$ = new Subject<void>();

  constructor(public genresService: GenresService) {}

  public ngOnInit(): void {
    this._getGenres();
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public changePage(page: number): void {
    this._getGenres(page);
  }

  private _getGenres(page: number = 1): void {
    this.genresService.getGenres(page)
      .pipe(
        takeUntil(this._destroy$),
      )
      .subscribe(({ genres, meta }: IPaginatedGenre) => {
        this.genres = genres;
        this.meta = meta;
      });
  } 

}

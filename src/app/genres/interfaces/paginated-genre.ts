import { IMeta } from 'src/app/pagination';

import { IGenre } from './genre';

export interface IPaginatedGenre {
    genres: IGenre[],
    meta: IMeta,
}

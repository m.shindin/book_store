import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent {

  @Input() public page: number = 1;
  @Input() public pages: number = 1;
  @Output() public currentPage = new EventEmitter<number>();

  constructor() {}

  public changePage(page: number): void {
    if(page !== this.pages + 1 && page !== 0) {
      this.currentPage.emit(page);
    }
  }

  public lengthOfPages(): any[] {
    const array = [];

    for(let index = 1; index <= this.pages; index++) {
      array.push(index);
    }

    return array;
  }

}

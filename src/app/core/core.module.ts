import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from '../app-routing.module';
import { GenresModule } from '../genres/genres.module';
import { BooksModule } from '../books/books.module';
import { AuthorsModule } from '../authors/authors.module';
import { LayoutModule } from '../layout/layout.module';
import { SearchModule } from '../search/search.module';

@NgModule({
  declarations: [],
  imports:[],
  exports:[
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    GenresModule,
    BooksModule,
    SearchModule,
    AuthorsModule,
    LayoutModule, 
    BrowserAnimationsModule,
  ],
})
export class CoreModule {}

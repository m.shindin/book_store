import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BookListComponent } from './components/book-list/book-list.component';
import { DetailBookComponent } from './components/detail-book/detail-book.component';
import { DetailBookResolver } from './resolvers/detail-book.resolver';

const routes: Routes = [
  { 
    path: '',
    component: BookListComponent,
  },
  { 
    path: ':id', component: DetailBookComponent, 
    resolve: 
    { 
      book: DetailBookResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BooksRoutingModule {}

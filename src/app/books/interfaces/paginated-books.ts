import { IMeta } from 'src/app/pagination';

import { IBook } from './book';

export interface IPaginatedBooks {
    books: IBook[],
    meta: IMeta
}

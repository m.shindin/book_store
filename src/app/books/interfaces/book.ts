import { IAuthor } from 'src/app/authors/interfaces/author';
import { IGenre } from 'src/app/genres';

export interface IBook {
  id: number,
  author: IAuthor,
  description: string,
  image: string,
  author_id: number,
  title: string,
  price: number,
  writing_date: Date,
  release_date: Date,
  genres: IGenre[],
}

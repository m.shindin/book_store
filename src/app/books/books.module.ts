import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { PaginationModule } from '../pagination';
import { SearchModule } from '../search/search.module';

import { BookListComponent } from './components/book-list/book-list.component';
import { BooksRoutingModule } from './books-routing.module';
import { DetailBookComponent } from './components/detail-book/detail-book.component';
import { AuthorFullNamePipe } from './pipes/author-full-name.pipe';

@NgModule({
  declarations: [BookListComponent, DetailBookComponent, AuthorFullNamePipe],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    PaginationModule,
    SearchModule,
    BooksRoutingModule,
    MatProgressSpinnerModule,
  ],
  exports: [BookListComponent],
})
export class BooksModule {}

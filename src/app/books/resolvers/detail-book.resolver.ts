import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
} from '@angular/router';

import { Observable } from 'rxjs';

import { IBook } from '../interfaces/book';
import { BooksService } from '../services/books.service';

@Injectable({
  providedIn: 'root',
})
export class DetailBookResolver implements Resolve<IBook> {

  constructor(
    private _booksService: BooksService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<IBook> {
    const id = Number(route.paramMap.get('id'));

    return this._booksService.getBook(id);
  }
}

import { Pipe, PipeTransform } from '@angular/core';

import { IAuthor } from 'src/app/authors/interfaces/author';

@Pipe({
  name: 'authorFullName',
})
export class AuthorFullNamePipe implements PipeTransform {

  public transform(value: IAuthor): string {
    return `${value.first_name} ${value.last_name}`;
  }

}

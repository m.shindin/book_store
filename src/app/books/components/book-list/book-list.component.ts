import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject, takeUntil, tap } from 'rxjs';

import { IMeta } from 'src/app/pagination';

import { BooksService } from '../../services/books.service';
import { IBook } from '../../interfaces/book';
import { IPaginatedBooks } from '../../interfaces/paginated-books';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
})
export class BookListComponent implements OnInit, OnDestroy {

  public books: IBook[] = [];
  public meta = <IMeta>{};
  public loading = false;
  public query = '';

  private readonly _destroy$ = new Subject<void>();

  constructor(public booksService: BooksService) {}

  public ngOnInit(): void {
    this._fetchBooks();
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public changePage(page: number): void {
    this._fetchBooks(page, this.query);
  }

  public search(query: string): void {
    this.query = query.trim();
    this.meta.page = 1;
    this.loading = true;
    this._fetchBooks(this.meta.page,this.query);
  }

  private _fetchBooks(page: number = 1, query: string = ''): void {
    this.booksService.getAllBooks(page, query)
      .pipe(
        tap(() => {
          this.loading = false;
        }),
        takeUntil(this._destroy$),
      )
      .subscribe(({ books, meta }: IPaginatedBooks) => { 
        this.books = books;
        this.meta = meta;
      });
  } 
  
}

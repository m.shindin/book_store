import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil, Subject } from 'rxjs';

import { IBook } from '../../interfaces/book';

@Component({
  selector: 'app-detail-book',
  templateUrl: './detail-book.component.html',
  styleUrls: ['./detail-book.component.css'],
})
export class DetailBookComponent implements OnInit, OnDestroy {
  
  public book = <IBook>{};
  
  private readonly _destroy$ = new Subject<void>();

  constructor(
    private _activeRoute: ActivatedRoute,
    private _route: Router,
  ) {}

  public ngOnInit(): void {
    this._getBook();
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public goBack(): void {
    this._route.navigateByUrl('/books');
  }

  private _getBook(): void {
    this._activeRoute.data
      .pipe(
        takeUntil(this._destroy$),
      )
      .subscribe(({ book }) => {
        this.book = book;
      },
      );
  }

}

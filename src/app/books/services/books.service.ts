import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IBook } from '../interfaces/book';
import { IPaginatedBooks } from '../interfaces/paginated-books';

@Injectable({
  providedIn: 'root',
})
export class BooksService {

  private _api = '/api';
  private _httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private _http: HttpClient,
  ) {}

  public getAllBooks(page: number = 1, query: string = ''): Observable<IPaginatedBooks> {
    return this._http.get<IPaginatedBooks>
    (`${this._api}/books/?page=${page}&q[title_cont]=${query}`);
  }

  public getBooksByAuthor(id: number): Observable<IBook[]> {
    return this._http.get<IBook[]>(`${this._api}/authors/${id}/books`);
  }

  public getBook(id: number): Observable<IBook> {
    const url = `${this._api}/books/${id}`;

    return this._http.get<IBook>(url);
  }

  public createBook(author: number, book: IBook): Observable<IBook> {
    const url = `${this._api}/authors/${author}/books`;

    return this._http.post<IBook>(url, book, this._httpOptions);
  }

  public updateBook(id: number): Observable<IBook> {
    const url = `${this._api}/books/${id}`;

    return this._http.put<IBook>(url, this._httpOptions);
  }

  public deleteBook(id: number): Observable<IBook> {
    const url = `${this._api}/books/${id}`;

    return this._http.delete<IBook>(url, this._httpOptions);
  }

}

import { Component, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  
  @Output() public menuOpened = new EventEmitter<void>();

  constructor() {}

  public toggleMenu() {
    this.menuOpened.emit();
  }

}

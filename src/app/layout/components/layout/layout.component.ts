import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
})
export class LayoutComponent {

  public isMenuOpened = false;

  constructor() {}

  public toggleMenu(): void {
    this.isMenuOpened = !(this.isMenuOpened);
  }
  
}

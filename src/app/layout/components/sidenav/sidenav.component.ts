import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
})
export class SidenavComponent {

  @Output() public menuOpened = new EventEmitter<void>();

  constructor() {}

  public toggleMenu() {
    this.menuOpened.emit();
  }

}
